Feature: Login feature

  Scenario: As a user I can peak Start
    Given I press "Start"
    Then I see "Target"

  Scenario: As a user I can peak Interval
    When I press "Start"
    And  I press "Interval"
    Then I see "Rest type"

  Scenario: As a user I can select Audio cue settings
    Given I press "Start"
    When  I press "Basic"
    And I press "Audio cue settings"
    And I press "100m"
    Then I see "100m"

  Scenario: As a user I can Biking as Sport
    Given I press "Start"
    When  I press "Basic"
    And I press "Sport"
    And I press "Biking"
    Then I see "Biking"

  Scenario: As a user I see About RunnerUp
    Given I press "Settings"
    When  I scroll down
    And I press "About RunnerUp"
    Then I see "RunnerUp"

  Scenario: As a user I can Edit Step
    Given I press "Start"
    When  I press "Advanced"
    And I press "warm up"
    And I press "Step intensity"
    And I press "Warmup"
    And I press "OK"
    Then I see "warm up"

  Scenario: As a user I can modify sensors
    Given I press "Settings"
    When  I press "Sensors"
    And I scroll down
    And I press "Headset key start/stop"
    And I press "Step sensor"
    And I press "Temperature sensor"
    And I press "Pressure sensor"
    And I go back
    And I press "Sensors"
    And I scroll down
    And I press "Headset key start/stop"
    And I press "Step sensor"
    And I press "Temperature sensor"
    And I press "Pressure sensor"
    And I go back
    Then I see "Sensors"

