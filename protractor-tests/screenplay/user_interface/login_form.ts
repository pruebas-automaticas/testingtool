import { by } from 'protractor';
import { Target } from 'serenity-js/lib/screenplay-protractor';

export class LoginForm {

  static usernameInput = Target.the('username input')
    .located(by.name('identification'));
  static passwordInput = Target.the('password input')
    .located(by.name('password'));
  static submitButton = Target.the('submit button')
    .located(by.xpath('*//button[@type="submit"]/span'));
  static errorCredentialsMessage = Target.the('Error credentials message')
    .located(by.className('main-error'));
}