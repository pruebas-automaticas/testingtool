import { Task } from "@serenity-js/core/lib/screenplay";
import { UseAngular, Enter, Click } from "serenity-js/lib/serenity-protractor";
import { Open } from "serenity-js/lib/screenplay-protractor";
import { WaitFor } from "../../../utilities/WaitFor";
import { LoginForm } from "../../user_interface/login_form";
import { MainMenu } from "../../user_interface/main_menu";

const Navigate = () => Task.where(`#actor opens the page`,
  UseAngular.disableSynchronisation(),
  Open.browserOn("/ghost/#/signin")
);

const EnterCredentials = (username:string, pass: string) => Task.where(`#actor enters the credentials`,
  WaitFor.visibilityOf(LoginForm.usernameInput),
  Enter.theValue(username)
    .into(LoginForm.usernameInput),

  WaitFor.visibilityOf(LoginForm.passwordInput),
  Enter.theValue(pass)
    .into(LoginForm.passwordInput),

  WaitFor.clickeabilityOf(LoginForm.submitButton),
  Click.on(LoginForm.submitButton)
);

const LogOut = () => Task.where(`#actor logout of the aplication`,
WaitFor.visibilityOf(MainMenu.menuDetailsUser),
Click.on(MainMenu.menuDetailsUser),
Click.on(MainMenu.menuSignOut)
);

export class DoLogin {

   static Navigate = () => Navigate();
   static EnterCredentials = (username:string, pass: string) => EnterCredentials(username, pass);
   static LogOut = () => LogOut();
}