const crew = require('serenity-js/lib/stage_crew');
require('ts-node/register');

exports.config = {

    baseUrl: 'http://localhost:2368',

    allScriptsTimeout: 110000,
    framework: 'custom',
    frameworkPath: require.resolve('serenity-js'),
    serenity: {
        dialect: 'cucumber',
        //keepAlive: false,
        crew: [
            crew.serenityBDDReporter(),
            crew.consoleReporter(),
            crew.Photographer.who(_ => _
                .takesPhotosOf(_.Tasks_and_Interactions)
                .takesPhotosWhen(_.Activity_Finishes)
            ),
        ]
    },

    specs: ['features/**/*.feature'],
    cucumberOpts: {
        require: ['features/**/*.ts'],
        format: 'pretty',
        compiler: 'ts:ts-node/register'
    },

    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
                'disable-extensions',
            ]
        }
    },
};
