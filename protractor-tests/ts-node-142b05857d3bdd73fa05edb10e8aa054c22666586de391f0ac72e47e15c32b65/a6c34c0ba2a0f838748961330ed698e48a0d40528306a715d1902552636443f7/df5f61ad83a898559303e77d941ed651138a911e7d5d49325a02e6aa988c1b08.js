"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var screenplay_1 = require("@serenity-js/core/lib/screenplay");
var serenity_protractor_1 = require("serenity-js/lib/serenity-protractor");
var step_annotation_1 = require("@serenity-js/core/lib/recording/step_annotation");
var defaultWaitTime = 5000000;
/**
 * Wrapper for the serenity waits due the explicit serenity wait  is too short.
 */
var WaitFor = /** @class */ (function () {
    function WaitFor() {
    }
    WaitFor.visibilityOf = function (target) { return new IsVisible(target); };
    WaitFor.clickeabilityOf = function (target) { return new IsClickeable(target); };
    WaitFor.invisibilityOf = function (target) { return new IsInvisible(target); };
    WaitFor.presenceOf = function (target) { return new IsPresent(target); };
    return WaitFor;
}());
exports.WaitFor = WaitFor;
var IsVisible = /** @class */ (function () {
    function IsVisible(target) {
        this.target = target;
    }
    IsVisible.prototype.performAs = function (actor) {
        return actor.attemptsTo(serenity_protractor_1.Wait.upTo(serenity_protractor_1.Duration.ofMillis(defaultWaitTime)).until(this.target, serenity_protractor_1.Is.visible()));
    };
    __decorate([
        step_annotation_1.step('{0} waits the element #target is visible.'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [typeof (_a = typeof screenplay_1.PerformsTasks !== "undefined" && screenplay_1.PerformsTasks) === "function" && _a || Object]),
        __metadata("design:returntype", Object)
    ], IsVisible.prototype, "performAs", null);
    return IsVisible;
    var _a;
}());
var IsClickeable = /** @class */ (function () {
    function IsClickeable(target) {
        this.target = target;
    }
    IsClickeable.prototype.performAs = function (actor) {
        return actor.attemptsTo(serenity_protractor_1.Wait.upTo(serenity_protractor_1.Duration.ofMillis(defaultWaitTime)).until(this.target, serenity_protractor_1.Is.clickable()));
    };
    __decorate([
        step_annotation_1.step('{0} waits the element #target is clickeable.'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [typeof (_b = typeof screenplay_1.PerformsTasks !== "undefined" && screenplay_1.PerformsTasks) === "function" && _b || Object]),
        __metadata("design:returntype", Object)
    ], IsClickeable.prototype, "performAs", null);
    return IsClickeable;
    var _b;
}());
var IsInvisible = /** @class */ (function () {
    function IsInvisible(target) {
        this.target = target;
    }
    IsInvisible.prototype.performAs = function (actor) {
        return actor.attemptsTo(serenity_protractor_1.Wait.upTo(serenity_protractor_1.Duration.ofMillis(defaultWaitTime)).until(this.target, serenity_protractor_1.Is.invisible()));
    };
    __decorate([
        step_annotation_1.step('{0} waits the element #target is invisible.'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [typeof (_c = typeof screenplay_1.PerformsTasks !== "undefined" && screenplay_1.PerformsTasks) === "function" && _c || Object]),
        __metadata("design:returntype", Object)
    ], IsInvisible.prototype, "performAs", null);
    return IsInvisible;
    var _c;
}());
var IsPresent = /** @class */ (function () {
    function IsPresent(target) {
        this.target = target;
    }
    IsPresent.prototype.performAs = function (actor) {
        return actor.attemptsTo(serenity_protractor_1.Wait.upTo(serenity_protractor_1.Duration.ofMillis(defaultWaitTime)).until(this.target, serenity_protractor_1.Is.present()));
    };
    __decorate([
        step_annotation_1.step('{0} waits the presence of #target element.'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [typeof (_d = typeof screenplay_1.PerformsTasks !== "undefined" && screenplay_1.PerformsTasks) === "function" && _d || Object]),
        __metadata("design:returntype", Object)
    ], IsPresent.prototype, "performAs", null);
    return IsPresent;
    var _d;
}());
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL1VzZXJzL3VycmVnb28vRG9jdW1lbnRzL1BlcnNvbmFsL1Byb3llY3Rvc19BdXRvbWF0aW9uL3Byb3RyYWN0b3ItdGVzdHMvdXRpbGl0aWVzL1dhaXRGb3IudHMiLCJzb3VyY2VzIjpbIi9Vc2Vycy91cnJlZ29vL0RvY3VtZW50cy9QZXJzb25hbC9Qcm95ZWN0b3NfQXV0b21hdGlvbi9wcm90cmFjdG9yLXRlc3RzL3V0aWxpdGllcy9XYWl0Rm9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsK0RBQW9GO0FBQ3BGLDJFQUFpRjtBQUNqRixtRkFBdUU7QUFFdkUsSUFBTSxlQUFlLEdBQUcsT0FBTyxDQUFDO0FBRWhDOztHQUVHO0FBQ0g7SUFBQTtJQU1BLENBQUM7SUFKUSxvQkFBWSxHQUFHLFVBQUMsTUFBYyxJQUFLLE9BQUEsSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQXJCLENBQXFCLENBQUM7SUFDekQsdUJBQWUsR0FBRyxVQUFDLE1BQWMsSUFBSyxPQUFBLElBQUksWUFBWSxDQUFDLE1BQU0sQ0FBQyxFQUF4QixDQUF3QixDQUFDO0lBQy9ELHNCQUFjLEdBQUcsVUFBQyxNQUFjLElBQUssT0FBQSxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUMsRUFBdkIsQ0FBdUIsQ0FBQztJQUM3RCxrQkFBVSxHQUFHLFVBQUMsTUFBYyxJQUFLLE9BQUEsSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQXJCLENBQXFCLENBQUM7SUFDaEUsY0FBQztDQUFBLEFBTkQsSUFNQztBQU5ZLDBCQUFPO0FBUXBCO0lBT0UsbUJBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBQ2xDLENBQUM7SUFORCw2QkFBUyxHQUFULFVBQVUsS0FBb0I7UUFDNUIsTUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQ3JCLDBCQUFJLENBQUMsSUFBSSxDQUFDLDhCQUFRLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsd0JBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUMvRSxDQUFDO0lBQ0osQ0FBQztJQUpEO1FBREMsc0JBQUksQ0FBQywyQ0FBMkMsQ0FBQzs7NkRBQ2pDLDBCQUFhLG9CQUFiLDBCQUFhOzs4Q0FJN0I7SUFHSCxnQkFBQzs7Q0FBQSxBQVRELElBU0M7QUFFRDtJQU9FLHNCQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUNsQyxDQUFDO0lBTkQsZ0NBQVMsR0FBVCxVQUFVLEtBQW9CO1FBQzVCLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUNyQiwwQkFBSSxDQUFDLElBQUksQ0FBQyw4QkFBUSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLHdCQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FDakYsQ0FBQztJQUNKLENBQUM7SUFKRDtRQURDLHNCQUFJLENBQUMsOENBQThDLENBQUM7OzZEQUNwQywwQkFBYSxvQkFBYiwwQkFBYTs7aURBSTdCO0lBR0gsbUJBQUM7O0NBQUEsQUFURCxJQVNDO0FBRUQ7SUFPRSxxQkFBb0IsTUFBYztRQUFkLFdBQU0sR0FBTixNQUFNLENBQVE7SUFDbEMsQ0FBQztJQU5ELCtCQUFTLEdBQVQsVUFBVSxLQUFvQjtRQUM1QixNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FDckIsMEJBQUksQ0FBQyxJQUFJLENBQUMsOEJBQVEsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSx3QkFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQ2pGLENBQUM7SUFDSixDQUFDO0lBSkQ7UUFEQyxzQkFBSSxDQUFDLDZDQUE2QyxDQUFDOzs2REFDbkMsMEJBQWEsb0JBQWIsMEJBQWE7O2dEQUk3QjtJQUdILGtCQUFDOztDQUFBLEFBVEQsSUFTQztBQUVEO0lBT0UsbUJBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBQ2xDLENBQUM7SUFORCw2QkFBUyxHQUFULFVBQVUsS0FBb0I7UUFDNUIsTUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQ3JCLDBCQUFJLENBQUMsSUFBSSxDQUFDLDhCQUFRLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsd0JBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUMvRSxDQUFDO0lBQ0osQ0FBQztJQUpEO1FBREMsc0JBQUksQ0FBQyw0Q0FBNEMsQ0FBQzs7NkRBQ2xDLDBCQUFhLG9CQUFiLDBCQUFhOzs4Q0FJN0I7SUFHSCxnQkFBQzs7Q0FBQSxBQVRELElBU0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbnRlcmFjdGlvbiwgVGFzaywgUGVyZm9ybXNUYXNrcyB9IGZyb20gJ0BzZXJlbml0eS1qcy9jb3JlL2xpYi9zY3JlZW5wbGF5JztcbmltcG9ydCB7IFRhcmdldCwgV2FpdCwgRHVyYXRpb24sIElzIH0gZnJvbSAnc2VyZW5pdHktanMvbGliL3NlcmVuaXR5LXByb3RyYWN0b3InO1xuaW1wb3J0IHsgc3RlcCB9IGZyb20gJ0BzZXJlbml0eS1qcy9jb3JlL2xpYi9yZWNvcmRpbmcvc3RlcF9hbm5vdGF0aW9uJztcblxuY29uc3QgZGVmYXVsdFdhaXRUaW1lID0gNTAwMDAwMDtcblxuLyoqXG4gKiBXcmFwcGVyIGZvciB0aGUgc2VyZW5pdHkgd2FpdHMgZHVlIHRoZSBleHBsaWNpdCBzZXJlbml0eSB3YWl0ICBpcyB0b28gc2hvcnQuXG4gKi9cbmV4cG9ydCBjbGFzcyBXYWl0Rm9yIHtcblxuICBzdGF0aWMgdmlzaWJpbGl0eU9mID0gKHRhcmdldDogVGFyZ2V0KSA9PiBuZXcgSXNWaXNpYmxlKHRhcmdldCk7XG4gIHN0YXRpYyBjbGlja2VhYmlsaXR5T2YgPSAodGFyZ2V0OiBUYXJnZXQpID0+IG5ldyBJc0NsaWNrZWFibGUodGFyZ2V0KTtcbiAgc3RhdGljIGludmlzaWJpbGl0eU9mID0gKHRhcmdldDogVGFyZ2V0KSA9PiBuZXcgSXNJbnZpc2libGUodGFyZ2V0KTtcbiAgc3RhdGljIHByZXNlbmNlT2YgPSAodGFyZ2V0OiBUYXJnZXQpID0+IG5ldyBJc1ByZXNlbnQodGFyZ2V0KTtcbn1cblxuY2xhc3MgSXNWaXNpYmxlIGltcGxlbWVudHMgVGFzayB7XG4gIEBzdGVwKCd7MH0gd2FpdHMgdGhlIGVsZW1lbnQgI3RhcmdldCBpcyB2aXNpYmxlLicpXG4gIHBlcmZvcm1BcyhhY3RvcjogUGVyZm9ybXNUYXNrcyk6IFByb21pc2VMaWtlPHZvaWQ+IHtcbiAgICByZXR1cm4gYWN0b3IuYXR0ZW1wdHNUbyhcbiAgICAgIFdhaXQudXBUbyhEdXJhdGlvbi5vZk1pbGxpcyhkZWZhdWx0V2FpdFRpbWUpKS51bnRpbCh0aGlzLnRhcmdldCwgSXMudmlzaWJsZSgpKSxcbiAgICApO1xuICB9XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdGFyZ2V0OiBUYXJnZXQpIHtcbiAgfVxufVxuXG5jbGFzcyBJc0NsaWNrZWFibGUgaW1wbGVtZW50cyBUYXNrIHtcbiAgQHN0ZXAoJ3swfSB3YWl0cyB0aGUgZWxlbWVudCAjdGFyZ2V0IGlzIGNsaWNrZWFibGUuJylcbiAgcGVyZm9ybUFzKGFjdG9yOiBQZXJmb3Jtc1Rhc2tzKTogUHJvbWlzZUxpa2U8dm9pZD4ge1xuICAgIHJldHVybiBhY3Rvci5hdHRlbXB0c1RvKFxuICAgICAgV2FpdC51cFRvKER1cmF0aW9uLm9mTWlsbGlzKGRlZmF1bHRXYWl0VGltZSkpLnVudGlsKHRoaXMudGFyZ2V0LCBJcy5jbGlja2FibGUoKSksXG4gICAgKTtcbiAgfVxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRhcmdldDogVGFyZ2V0KSB7XG4gIH1cbn1cblxuY2xhc3MgSXNJbnZpc2libGUgaW1wbGVtZW50cyBUYXNrIHtcbiAgQHN0ZXAoJ3swfSB3YWl0cyB0aGUgZWxlbWVudCAjdGFyZ2V0IGlzIGludmlzaWJsZS4nKVxuICBwZXJmb3JtQXMoYWN0b3I6IFBlcmZvcm1zVGFza3MpOiBQcm9taXNlTGlrZTx2b2lkPiB7XG4gICAgcmV0dXJuIGFjdG9yLmF0dGVtcHRzVG8oXG4gICAgICBXYWl0LnVwVG8oRHVyYXRpb24ub2ZNaWxsaXMoZGVmYXVsdFdhaXRUaW1lKSkudW50aWwodGhpcy50YXJnZXQsIElzLmludmlzaWJsZSgpKSxcbiAgICApO1xuICB9XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdGFyZ2V0OiBUYXJnZXQpIHtcbiAgfVxufVxuXG5jbGFzcyBJc1ByZXNlbnQgaW1wbGVtZW50cyBUYXNrIHtcbiAgQHN0ZXAoJ3swfSB3YWl0cyB0aGUgcHJlc2VuY2Ugb2YgI3RhcmdldCBlbGVtZW50LicpXG4gIHBlcmZvcm1BcyhhY3RvcjogUGVyZm9ybXNUYXNrcyk6IFByb21pc2VMaWtlPHZvaWQ+IHtcbiAgICByZXR1cm4gYWN0b3IuYXR0ZW1wdHNUbyhcbiAgICAgIFdhaXQudXBUbyhEdXJhdGlvbi5vZk1pbGxpcyhkZWZhdWx0V2FpdFRpbWUpKS51bnRpbCh0aGlzLnRhcmdldCwgSXMucHJlc2VudCgpKSxcbiAgICApO1xuICB9XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdGFyZ2V0OiBUYXJnZXQpIHtcbiAgfVxufVxuXG4iXX0=