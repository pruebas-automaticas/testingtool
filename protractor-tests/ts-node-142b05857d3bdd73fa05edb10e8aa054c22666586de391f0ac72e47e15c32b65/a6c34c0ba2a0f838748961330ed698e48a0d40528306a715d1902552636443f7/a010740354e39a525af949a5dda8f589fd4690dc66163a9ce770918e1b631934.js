"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var screenplay_protractor_1 = require("serenity-js/lib/screenplay-protractor");
var LoginForm = /** @class */ (function () {
    function LoginForm() {
    }
    LoginForm.usernameInput = screenplay_protractor_1.Target.the('username input')
        .located(protractor_1.by.name('identification'));
    LoginForm.passwordInput = screenplay_protractor_1.Target.the('password input')
        .located(protractor_1.by.name('password'));
    LoginForm.submitButton = screenplay_protractor_1.Target.the('submit button')
        .located(protractor_1.by.xpath('*//button[@type="submit"]/span'));
    LoginForm.invalidCredentialsMessage = screenplay_protractor_1.Target.the('Invalid credentials message')
        .located(protractor_1.by.css('p.error'));
    return LoginForm;
}());
exports.LoginForm = LoginForm;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL1VzZXJzL3VycmVnb28vRG9jdW1lbnRzL1BlcnNvbmFsL1Byb3llY3Rvc19BdXRvbWF0aW9uL3Byb3RyYWN0b3ItdGVzdHMvc2NyZWVucGxheS91c2VyX2ludGVyZmFjZS9sb2dpbl9mb3JtLnRzIiwic291cmNlcyI6WyIvVXNlcnMvdXJyZWdvby9Eb2N1bWVudHMvUGVyc29uYWwvUHJveWVjdG9zX0F1dG9tYXRpb24vcHJvdHJhY3Rvci10ZXN0cy9zY3JlZW5wbGF5L3VzZXJfaW50ZXJmYWNlL2xvZ2luX2Zvcm0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx5Q0FBZ0M7QUFDaEMsK0VBQStEO0FBRS9EO0lBQUE7SUFVQSxDQUFDO0lBUlEsdUJBQWEsR0FBRyw4QkFBTSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQztTQUNoRCxPQUFPLENBQUMsZUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7SUFDL0IsdUJBQWEsR0FBRyw4QkFBTSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQztTQUNoRCxPQUFPLENBQUMsZUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQ3pCLHNCQUFZLEdBQUcsOEJBQU0sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDO1NBQzlDLE9BQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQztJQUNoRCxtQ0FBeUIsR0FBRyw4QkFBTSxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQztTQUN6RSxPQUFPLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQ2hDLGdCQUFDO0NBQUEsQUFWRCxJQVVDO0FBVlksOEJBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBieSB9IGZyb20gJ3Byb3RyYWN0b3InO1xuaW1wb3J0IHsgVGFyZ2V0IH0gZnJvbSAnc2VyZW5pdHktanMvbGliL3NjcmVlbnBsYXktcHJvdHJhY3Rvcic7XG5cbmV4cG9ydCBjbGFzcyBMb2dpbkZvcm0ge1xuXG4gIHN0YXRpYyB1c2VybmFtZUlucHV0ID0gVGFyZ2V0LnRoZSgndXNlcm5hbWUgaW5wdXQnKVxuICAgIC5sb2NhdGVkKGJ5Lm5hbWUoJ2lkZW50aWZpY2F0aW9uJykpO1xuICBzdGF0aWMgcGFzc3dvcmRJbnB1dCA9IFRhcmdldC50aGUoJ3Bhc3N3b3JkIGlucHV0JylcbiAgICAubG9jYXRlZChieS5uYW1lKCdwYXNzd29yZCcpKTtcbiAgc3RhdGljIHN1Ym1pdEJ1dHRvbiA9IFRhcmdldC50aGUoJ3N1Ym1pdCBidXR0b24nKVxuICAgIC5sb2NhdGVkKGJ5LnhwYXRoKCcqLy9idXR0b25bQHR5cGU9XCJzdWJtaXRcIl0vc3BhbicpKTtcbiAgc3RhdGljIGludmFsaWRDcmVkZW50aWFsc01lc3NhZ2UgPSBUYXJnZXQudGhlKCdJbnZhbGlkIGNyZWRlbnRpYWxzIG1lc3NhZ2UnKVxuICAgIC5sb2NhdGVkKGJ5LmNzcygncC5lcnJvcicpKTtcbn0iXX0=