"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = /** @class */ (function () {
    function User() {
    }
    Object.defineProperty(User.prototype, "Username", {
        get: function () {
            return this._username;
        },
        set: function (value) {
            this._username = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "Pass", {
        get: function () {
            return this._pass;
        },
        set: function (value) {
            this._pass = value;
        },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;
exports.GetValidUser = function () {
    var user = new User();
    user.Username = 'oscarbuho888@gmail.com';
    user.Pass = '20051005112';
    return user;
};
exports.GetInvalidUser = function () {
    var user = new User();
    user.Username = 'bvi77777';
    user.Pass = 'Pruebas1';
    return user;
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL1VzZXJzL3VycmVnb28vRG9jdW1lbnRzL1BlcnNvbmFsL1Byb3llY3Rvc19BdXRvbWF0aW9uL3Byb3RyYWN0b3ItdGVzdHMvZGF0YS9HZXRVc2VyLnRzIiwic291cmNlcyI6WyIvVXNlcnMvdXJyZWdvby9Eb2N1bWVudHMvUGVyc29uYWwvUHJveWVjdG9zX0F1dG9tYXRpb24vcHJvdHJhY3Rvci10ZXN0cy9kYXRhL0dldFVzZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtJQUFBO0lBb0JBLENBQUM7SUFmQyxzQkFBSSwwQkFBUTthQUlaO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDeEIsQ0FBQzthQU5ELFVBQWEsS0FBSztZQUNoQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN6QixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLHNCQUFJO2FBSVI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNwQixDQUFDO2FBTkQsVUFBUyxLQUFLO1lBQ1osSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDckIsQ0FBQzs7O09BQUE7SUFLSCxXQUFDO0FBQUQsQ0FBQyxBQXBCRCxJQW9CQztBQXBCWSxvQkFBSTtBQXNCSixRQUFBLFlBQVksR0FBRztJQUMxQixJQUFNLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO0lBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsd0JBQXdCLENBQUM7SUFDekMsSUFBSSxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7SUFDMUIsTUFBTSxDQUFDLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQTtBQUVZLFFBQUEsY0FBYyxHQUFHO0lBQzVCLElBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7SUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7SUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7SUFDdkIsTUFBTSxDQUFDLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBVc2VyIHtcblxuICBwcml2YXRlIF91c2VybmFtZTogc3RyaW5nO1xuICBwcml2YXRlIF9wYXNzOiBzdHJpbmc7XG5cbiAgc2V0IFVzZXJuYW1lKHZhbHVlKSB7XG4gICAgdGhpcy5fdXNlcm5hbWUgPSB2YWx1ZTtcbiAgfVxuXG4gIGdldCBVc2VybmFtZSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLl91c2VybmFtZTtcbiAgfVxuXG4gIHNldCBQYXNzKHZhbHVlKSB7XG4gICAgdGhpcy5fcGFzcyA9IHZhbHVlO1xuICB9XG5cbiAgZ2V0IFBhc3MoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5fcGFzcztcbiAgfVxufVxuXG5leHBvcnQgY29uc3QgR2V0VmFsaWRVc2VyID0gKCkgPT4ge1xuICBjb25zdCB1c2VyID0gbmV3IFVzZXIoKTtcbiAgdXNlci5Vc2VybmFtZSA9ICdvc2NhcmJ1aG84ODhAZ21haWwuY29tJztcbiAgdXNlci5QYXNzID0gJzIwMDUxMDA1MTEyJztcbiAgcmV0dXJuIHVzZXI7XG59XG5cbmV4cG9ydCBjb25zdCBHZXRJbnZhbGlkVXNlciA9ICgpID0+IHtcbiAgY29uc3QgdXNlciA9IG5ldyBVc2VyKCk7XG4gIHVzZXIuVXNlcm5hbWUgPSAnYnZpNzc3NzcnO1xuICB1c2VyLlBhc3MgPSAnUHJ1ZWJhczEnO1xuICByZXR1cm4gdXNlcjtcbn1cbiJdfQ==