"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = /** @class */ (function () {
    function User() {
    }
    Object.defineProperty(User.prototype, "Username", {
        get: function () {
            return this._username;
        },
        set: function (value) {
            this._username = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "Pass", {
        get: function () {
            return this._pass;
        },
        set: function (value) {
            this._pass = value;
        },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;
exports.GetValidUser = function () {
    var user = new User();
    user.Username = 'bvi4335';
    user.Pass = 'Pruebas1';
    return user;
};
exports.GetInvalidUser = function () {
    var user = new User();
    user.Username = 'bvi77777';
    user.Pass = 'Pruebas1';
    return user;
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL1VzZXJzL3VycmVnb28vRG9jdW1lbnRzL1BlcnNvbmFsL1Byb3llY3Rvc19BdXRvbWF0aW9uL3Byb3RyYWN0b3ItdGVzdHMvZGF0YS9HZXRVc2VyLnRzIiwic291cmNlcyI6WyIvVXNlcnMvdXJyZWdvby9Eb2N1bWVudHMvUGVyc29uYWwvUHJveWVjdG9zX0F1dG9tYXRpb24vcHJvdHJhY3Rvci10ZXN0cy9kYXRhL0dldFVzZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtJQUFBO0lBb0JBLENBQUM7SUFmQyxzQkFBSSwwQkFBUTthQUlaO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDeEIsQ0FBQzthQU5ELFVBQWEsS0FBSztZQUNoQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN6QixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLHNCQUFJO2FBSVI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNwQixDQUFDO2FBTkQsVUFBUyxLQUFLO1lBQ1osSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDckIsQ0FBQzs7O09BQUE7SUFLSCxXQUFDO0FBQUQsQ0FBQyxBQXBCRCxJQW9CQztBQXBCWSxvQkFBSTtBQXNCSixRQUFBLFlBQVksR0FBRztJQUMxQixJQUFNLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO0lBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO0lBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO0lBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUE7QUFFWSxRQUFBLGNBQWMsR0FBRztJQUM1QixJQUFNLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO0lBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO0lBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO0lBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgVXNlciB7XG5cbiAgcHJpdmF0ZSBfdXNlcm5hbWU6IHN0cmluZztcbiAgcHJpdmF0ZSBfcGFzczogc3RyaW5nO1xuXG4gIHNldCBVc2VybmFtZSh2YWx1ZSkge1xuICAgIHRoaXMuX3VzZXJuYW1lID0gdmFsdWU7XG4gIH1cblxuICBnZXQgVXNlcm5hbWUoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5fdXNlcm5hbWU7XG4gIH1cblxuICBzZXQgUGFzcyh2YWx1ZSkge1xuICAgIHRoaXMuX3Bhc3MgPSB2YWx1ZTtcbiAgfVxuXG4gIGdldCBQYXNzKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuX3Bhc3M7XG4gIH1cbn1cblxuZXhwb3J0IGNvbnN0IEdldFZhbGlkVXNlciA9ICgpID0+IHtcbiAgY29uc3QgdXNlciA9IG5ldyBVc2VyKCk7XG4gIHVzZXIuVXNlcm5hbWUgPSAnYnZpNDMzNSc7XG4gIHVzZXIuUGFzcyA9ICdQcnVlYmFzMSc7XG4gIHJldHVybiB1c2VyO1xufVxuXG5leHBvcnQgY29uc3QgR2V0SW52YWxpZFVzZXIgPSAoKSA9PiB7XG4gIGNvbnN0IHVzZXIgPSBuZXcgVXNlcigpO1xuICB1c2VyLlVzZXJuYW1lID0gJ2J2aTc3Nzc3JztcbiAgdXNlci5QYXNzID0gJ1BydWViYXMxJztcbiAgcmV0dXJuIHVzZXI7XG59XG4iXX0=